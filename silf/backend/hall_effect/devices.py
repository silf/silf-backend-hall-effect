import os
import time
import random
from silf.backend.drivers.power_hantek.hantek_pps2116a_device_nc import HantekPPS2116ADeviceNC
from silf.drivers.usbtmc.rigol_dm_3000_device import RigolDM3000UsbtmcDeviceNC

class HantekH (HantekPPS2116ADeviceNC):
    device_id = "hantekH"

class HantekB (HantekPPS2116ADeviceNC):
    device_id = "hantekB"

class RigolVoltage(RigolDM3000UsbtmcDeviceNC):
    device_id = "RigolVoltage"


class MockDevice:

    def __init__(self):
        self.num = -1

    def init(self, config):
        print('in init for MockDevice: config =', config)

    def power_up(self):
        time.sleep(.5)
        print('power_up for MockDevice')

    def start_measurements(self):
        print('start_measurements for MockDevice')

    def apply_settings(self, settings):
        print('apply settings for MockDevice enter')
        time.sleep(0.5)
        self.v_from = settings['v_from']
        self.v_to = settings['v_to']
        self.v_step = settings['v_step']
        self.v = settings['voltage']
        print('apply settings for MockDevice exit')

    def read_state(self):
        time.sleep(0.5)
        return {'voltage': self.v, 'current': random.uniform(self.v_from, self.v_to)}

    def stop_measurements(self):
        print('stop_measurements for MockDevice')

    def power_down(self):
        print('power_down for MockDevice')
