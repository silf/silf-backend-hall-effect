
import os
import logging

from silf.backend.hall_effect.devices import HantekH, HantekB, RigolVoltage, MockDevice
from silf.backend.commons.new.result import ChartXYCreator
from silf.backend.commons.util.math_utils import round_sig

from silf.backend.commons.api.stanza_content import ControlSuite, NumberControl, \
    OutputFieldSuite, OutputField, ChartField

from silf.backend.commons.new.experiment import Experiment
from silf.backend.commons.new.experiment_mode import ExperimentMode


logging.basicConfig(level=logging.INFO)


class CurrentMode(ExperimentMode):

    name = 'Prąd'
    label = 'Sterowanie prądem halotronu'

    controls = ControlSuite(
        NumberControl("v_from", "Poczatkowa wartość prądu m[A]", min_value=0, max_value=14, step=1, default_value=0),
        NumberControl("v_to", "Końcowa wartość prądu [mA]", min_value=15, max_value=30, step=1, default_value=30),
        NumberControl("v_step", "Przyrost prądu [mA]", min_value=1, max_value=5, step=1, default_value=1),
        NumberControl("v_B", "Indukcjia pola magnetycznego [mT]", min_value=0, max_value=0.5, step=0.1, default_value=0.3),
    )

    output_fields = OutputFieldSuite(
        chart=ChartField("chart", ["chart"], "Efekt Halla", "Prąd halotronu [A]", "Napięcie Halla [V]"),
    )

    result_creators = [
        ChartXYCreator(name='chart', x='current_H', y='rigol_voltage')
    ]

    devices = [HantekH, HantekB, RigolVoltage]
    #devices = [MockDevice]

    def get_series_label(self, settings):
        return 'from {} to {}'.format(settings['v_from'].value, settings['v_to'].value)

    def next_step_settings(self, settings, results):
        settings['v_from'] = self.settings['v_from']
        settings['v_to'] = self.settings['v_to']
        settings['current_H'] =  round_sig(self.settings['v_from']+settings.get('step', 0)*self.settings['v_step'], 4)/1000
        settings['current_B'] = self.settings['v_B']

        print(results)
        print(settings)

        if settings.get('step', 0) > (settings['v_to'] - settings['v_from'])/settings['v_step']:
            return None
        else:
            return settings

class MagnetMode(ExperimentMode):

    name = 'Indukcja'
    label = 'Sterowanie polem magnetycznym'

    controls = ControlSuite(
        NumberControl("v_from", "Poczatkowa wartość indukcji pola [T]", min_value=0, max_value=0.09, step=0.01, default_value=0),
        NumberControl("v_to", "Końcowa wartość indukcji pola [T]", min_value=0.1, max_value=0.3, step=0.01, default_value=0.1),
        NumberControl("v_step", "Przyrost indukcji [T]", min_value=0.001, max_value=0.02, step=0.001, default_value=0.005),
        NumberControl("v_B", "Prąd halotronu [mA]", min_value=0, max_value=30, step=1, default_value=20),
    )

    output_fields = OutputFieldSuite(
        chart=ChartField("chart", ["chart"], "Efekt Halla", "Indukcja pola magnetycznego [T]", "Napięcie Halla [V]"),
    )

    result_creators = [
        ChartXYCreator(name='chart', x='current_B', y='rigol_voltage')
    ]

    devices = [HantekH, HantekB, RigolVoltage]
    #devices = [MockDevice]

    def get_series_label(self, settings):
        return 'from {} to {}'.format(settings['v_from'].value, settings['v_to'].value)

    def next_step_settings(self, settings, results):
        settings['v_from'] = self.settings['v_from']
        settings['v_to'] = self.settings['v_to']
        settings['current_B'] =  round_sig(self.settings['v_from']+settings.get('step', 0)*self.settings['v_step'], 4)
        settings['current_H'] = self.settings['v_B']/1000

        print(results)
        print(settings)

        if settings.get('step', 0) > (settings['v_to'] - settings['v_from'])/settings['v_step']:
            return None
        else:
            return settings


class HallExperiment(Experiment):

    modes = [CurrentMode, MagnetMode]

    def set_mode(self, mode):
        print(mode)
