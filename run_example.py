# -*- coding: utf-8 -*-

import threading
import time
from circuits import Debugger
from silf.backend.hall_effect.experiment import HallExperiment
from silf.backend.client.mock_client import TestClient
from silf.backend.commons.api import ModeSelection, SettingSuite, Setting, SeriesStopped

if __name__ == "__main__":
    experiment = HallExperiment(config='experiment.ini', client=TestClient('experiment.ini', section='XMPPClient')) + Debugger()
    client = experiment.connection.client
    t = threading.Thread(target=lambda: experiment.run())
    t.start()
    client.fire_synthetic_event("silf:mode:get", "query")
    client.fire_synthetic_event("silf:mode:set", "query", suite=ModeSelection(mode="average"))
    time.sleep(.5)
    client.fire_synthetic_event("silf:series:start", "query", suite=SettingSuite(num_from=Setting(2), num_to=Setting(5)))
    time.sleep(2)
    client.fire_synthetic_event("silf:settings:update", "query", suite=SettingSuite(light=Setting(True)))
    time.sleep(1)
    experiment.stop()
    print(client.sent_stanzas)
    for error in client.sent_errors:
        print(error)

